import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class StudentForm {

    private static final int MIN_YEAR_OF_BIRTH = LocalDate.now().getYear() - 35;
    private static final int MAX_YEAR_OF_BIRTH = LocalDate.now().getYear() - 16;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String firstName = getValidFirstName(scanner);

        String lastName = getValidLastName(scanner);

        LocalDate birthDate = getValidBirthDate(scanner);

        String course = getValidCourse(scanner);

        String email = getValidEmail(scanner);

        String studentNumber = generateStudentNumber(birthDate, lastName);
        System.out.println("Your student number is: " + studentNumber);
    }

    private static String getValidFirstName(Scanner scanner) {
        String firstName;
        do {
            System.out.print("Enter your first name: ");
            firstName = scanner.nextLine().trim();
        } while (!firstName.matches("[A-Za-z ]+") || firstName.length() > 50);
        return firstName;
    }

    private static String getValidLastName(Scanner scanner) {
        String lastName;
        do {
            System.out.print("Enter your last name: ");
            lastName = scanner.nextLine().trim();
        } while (!lastName.matches("[A-Za-z ]+") || lastName.length() > 50);
        return lastName;
    }

    private static LocalDate getValidBirthDate(Scanner scanner) {
        LocalDate birthDate;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        do {
            System.out.print("Enter your date of birth (yyyy/mm/dd): ");
            String input = scanner.nextLine().trim();
            try {
                birthDate = LocalDate.parse(input, formatter);
                int year = birthDate.getYear();
                if (year < MIN_YEAR_OF_BIRTH || year > MAX_YEAR_OF_BIRTH) {
                    System.out.println("You must be between 16 and 25 years old to register.");
                    birthDate = null;
                }
            } catch (Exception e) {
                birthDate = null;
            }
        } while (birthDate == null);
        return birthDate;
    }

    private static String getValidCourse(Scanner scanner) {
        String course;
        do {
            System.out.print("Enter your course: ");
            course = scanner.nextLine().trim();
        } while (course.matches(".*\\d.*") || course.length() > 50);
        return course;
    }

    private static String getValidEmail(Scanner scanner) {
        String email;
        do {
            System.out.print("Enter your email address: ");
            email = scanner.nextLine().trim();
        } while (!email.matches("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"));
        return email;
    }

    private static String generateStudentNumber(LocalDate birthDate, String lastName) {
        int year = LocalDate.now().getYear();
        String yearString = Integer.toString(year);
        String birthDateString = birthDate.format(DateTimeFormatter.ofPattern("MMdd"));
        String suffix = lastName.substring(0, 1).toLowerCase();
        return yearString + "-" + birthDateString + "-01-" + suffix;
    }
}
