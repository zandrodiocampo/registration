import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Student {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] studentDetails = new String[4];

        System.out.println("First Name: (must not exceed 50 characters long and no numbers or special characters)");
        String firstName = scanner.nextLine();

        // I included spaces in firstname since some names have multiple words
        Pattern pattern = Pattern.compile("^[a-zA-Z_ ]{1,50}$",Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(firstName);
        boolean matchfound = matcher.find();

        if (!matchfound){
            System.out.println("Enter a valid FirstName");
        } else {
            System.out.println(matchfound);
            studentDetails[0] = firstName;
        }

        System.out.println("Last Name:");
        String lastName = scanner.nextLine();

        Pattern pattern2 = Pattern.compile("^[a-zA-Z_ ]{1,50}$",Pattern.CASE_INSENSITIVE);
        Matcher matcher2 = pattern2.matcher(lastName);
        boolean matchfound2 = matcher2.find();
        if (!matchfound2){
            System.out.println("Enter a valid Last name");
        } else {
            System.out.println(matchfound2);
            studentDetails[1] = lastName;
        }



        for (int i = 0; i < studentDetails.length; i++) {
            System.out.println(studentDetails[i]);
        }



    }
}